package com.information.info.service;


import com.information.info.entity.Info;
import com.information.info.repository.InfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class InfoService {
    @Autowired
    private InfoRepository infoRepository;
    @Autowired
    private EntityManager em;
    public List<Info> infos(Integer flag){
        return  infoRepository.findByFlag(flag);
    }
    public Info infoByflag(Long id,Integer flag){
        return infoRepository.findByIdAndFlag(id,flag).orElseThrow(() -> new IllegalArgumentException("资讯不存在或已下架"));
    }
    public Info info(Long id){
        return infoRepository.findById(id).orElseThrow(()->new IllegalArgumentException("资讯不存在"));
    }

    @Transactional
    public Info add(Info info){
        log.info("product to be saved: {}", info);
        info.setFlag(0);
        infoRepository.save(info);
        em.flush();
        Long i=infoRepository.max();
        System.out.println(i);
        if(i==null) i=new Long(1);
        System.out.println("sort的排序:"+i);
        info.setSort(i);
        infoRepository.save(info);
        em.flush();
        return info;
    }
    @Transactional
    public Info up(Long id){
        Info info=infoRepository.findOne(id);
        if(info==null){
            throw new IllegalArgumentException("资讯不存在");
        }
        info.setFlag(new Integer(1));
        infoRepository.save(info);
        em.flush();
        log.info("资讯修改状态为展览状态：{}"+info);
        return info;
    }
    @Transactional
    public Info down(Long id){
        Info info=infoRepository.findOne(id);
        if(info==null){
            throw new IllegalArgumentException("资讯不存在");
        }
        info.setFlag(new Integer(0));
        infoRepository.save(info);
        em.flush();
        log.info("资讯修改状态为下架状态：{}"+info);
        return info;
    }

    @Transactional
    public Info update(Info info){
        Info tmp=infoRepository.findOne(info.getId());
        if(tmp==null){
            throw new IllegalArgumentException("资讯不存在");
        }
        infoRepository.save(info);
        em.flush();
        return info;
    }
    public void deleteInfo(Long id){
        infoRepository.delete(id);
    }

    public List<Info> infosByPage(Pageable pageRequest, Integer flag){
        return infoRepository.infosBypage(flag,pageRequest);
    }

    public List<Info> infosByPageAndSort(Pageable pageable,Integer flag){
        return  infoRepository.infosByPageAndSortAndFlag(flag,pageable);
    }

    @Transactional
    public List<Info> updatesort(Pageable pageable,Integer flag,List<Info> infoList,List<Long> idlist){
        List<Long> sortList=new ArrayList<Long>();
        for(Info i:infoList){
            sortList.add(i.getSort());
        }
        for(int i=0;i<idlist.size();i++)
        {
            Info info=new Info();
            for(int j=0;j<infoList.size();j++){
                if(infoList.get(j).getId().equals(idlist.get(i))) {
                    info=infoList.get(j);
                    break;
                }
            }
            info.setSort(sortList.get(i));
        }
        for(Info info:infoList){
            infoRepository.save(info);
        }
        em.flush();
        return this.infosByPageAndSort(pageable,flag);
    }
    public List<Info> search(String keyword){
        return infoRepository.search(keyword);
    }
}
