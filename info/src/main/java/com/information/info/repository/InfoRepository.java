package com.information.info.repository;

import com.information.info.entity.Info;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

@Repository
public interface InfoRepository extends CrudRepository<Info,Long>{
     Optional<Info> findByIdAndFlag(Long id,Integer flag);
     List<Info> findByFlag(Integer flag);

     Optional<Info> findById(Long id);

    /**
     * 分页查询资讯（按是否是展览状态）
     * @param flag
     * @param pageable
     * @return
     */
    @Query("SELECT i FROM Info i WHERE i.flag= ?1")
     List<Info> infosBypage(Integer flag,Pageable pageable);

    @Query("SELECT i FROM Info i WHERE i.title  LIKE CONCAT('%',?1,'%')")
     List<Info> search(String keyword);

    @Query("SELECT i FROM Info i WHERE i.flag=?1")
     List<Info> infosByPageAndSortAndFlag(Integer flag,Pageable pageable);

    @Query("SELECT MAX(i.id) from Info i")
     Long max();
}
