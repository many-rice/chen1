package com.information.info.controller.admin;


import com.information.info.entity.Info;
import com.information.info.service.InfoService;
import lombok.extern.slf4j.Slf4j;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/information/admin")
@Slf4j
public class InfoController {
    @Autowired
    private InfoService infoService;

    @Value("${upload.path}")
    private String path;

    /**
     * 添加资讯
     * @param info
     */
    @PostMapping("/info")
    public Info add(@RequestBody Info info){
        return infoService.add(info);
    }

    /**
     * 上传资讯图片
     * @param file
     * @return
     */
    @PostMapping(value = "/images", produces = "application/json; charset=utf-8")
    public Info imgUpdate(@RequestParam(value = "file") MultipartFile file){
        if(file.isEmpty()){
            throw new IllegalArgumentException("图片不能为空");
        }
        String fileName=file.getOriginalFilename();
        File dest=new File(path+fileName);
        log.info("图片上传后的路径为:{}",dest);
        if(!dest.getParentFile().exists()){
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
            log.info("图片已上传:{}",dest);
            return Info.builder().album(dest.getAbsolutePath()).build();
        }catch(IOException e){
            log.error("图片上传失败:{}",dest);
            log.error("{}",e.getCause());
        }
        throw new IllegalArgumentException("文件上传失败");
    }

    /**
     * 按（资讯是否是展览状态）查询所有资讯
     * flag=0，不在展览状态
     * flag=1，在展览状态
     * @param pageNumber
     * @param pageSize
     * @param flag
     * @return
     */
    @GetMapping("/infos")
    public Iterable<Info> infos(@RequestParam("pageNumber") Integer pageNumber,
                                @RequestParam("pageSize") Integer pageSize,
                                @RequestParam("flag")  Integer flag){
        return infoService.infosByPage(buildPageRequest(pageNumber,pageSize,null),flag);
    }

    /**
     * 修改资讯状态，把状态flag置为1
     * @param id
     * @return
     */
    @PutMapping("/up/{id}")
    public Info up(@PathVariable("id") Long id){
        return infoService.up(id);
    }

    /**
     * 修改资讯状态，把状态flag置为0
     * @param id
     * @return
     */
    @DeleteMapping("/down/{id}")
    public Info down(@PathVariable("id") Long id){
        return infoService.down(id);
    }

    /**
     * 删除资讯
     * @param id
     */
    @DeleteMapping("/info/{id}")
    public void deleteinfo(Long id){
        infoService.deleteInfo(id);
    }

    /**
     * 按标题模糊查询
     * @param keyword
     * @return
     */
    @GetMapping("/search")
    public Iterable<Info> search(@RequestParam("keyword") String keyword){
        return infoService.search(keyword);
    }


    /**
     * 按展示状态（flag=0 or 1）进行排序，分页查询
     * @param pageNumber
     * @param pageSize
     * @param flag
     * @return
     */
    @GetMapping("/sort")
    public Iterable<Info> sort(@RequestParam("pageNumber") Integer pageNumber,
                               @RequestParam("pageSize") Integer pageSize,
                               @RequestParam("flag") Integer flag){
        return infoService.infosByPageAndSort(new PageRequest(pageNumber, pageSize, new Sort(Sort.Direction.ASC, "sort")),flag);
    }

    /**
     * 按id查询资讯
     * @param id
     * @return
     */
    @GetMapping("/info/{id}")
    public Info info(@PathVariable("id") Long id){
        return infoService.info(id);
    }

    /**
     * 更新资讯
     * @param info
     * @return
     */
    @PutMapping("/update")
    public Info info(@RequestBody Info info){
        return infoService.update(info);
    }

    @PutMapping("/updatesort")
    public Iterable<Info> updatesort(@RequestBody List<Long> idlist, @RequestParam("pageNumber") Integer pn,@RequestParam("pageSize") Integer ps){
        PageRequest Pageable=new PageRequest(pn, ps, new Sort(Sort.Direction.ASC, "sort"));
        List<Info> infoList=infoService.infosByPageAndSort(Pageable,0);
        return infoService.updatesort(Pageable,new Integer(0),infoList,idlist);
    }

    public static PageRequest buildPageRequest(int pageNumber, int pageSize,Sort sort) {
        return new PageRequest(pageNumber, pageSize, sort);
    }
}
