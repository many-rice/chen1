package com.information.info.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.information.info.controller.vo.AuthorityVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import okhttp3.OkHttpClient;
import okhttp3.Request;


import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@Configuration
public class InfoInterceptor implements HandlerInterceptor,EnvironmentAware {

    private OkHttpClient client = new OkHttpClient();

    private ObjectMapper objectMapper = new ObjectMapper();

    private static String url;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = String.format("Bearer %s", request.getHeader("token"));
        log.info("token is {}", token);
        Request okRequest = new Request.Builder()
                .url(url)
                .addHeader("token", String.format("Bearer %s", request.getHeader("token")))
                .build();
        String resultString = client.newCall(okRequest).execute().body().string();
        log.info("result string is:{}", resultString);
        AuthorityVo authorityVo = null;
        try {
            authorityVo = objectMapper.reader().forType(AuthorityVo.class).readValue(resultString);
        } catch (Exception e) {
            log.error("error of authority", e.getCause());
        }
        if (authorityVo != null) {
            List<AuthorityVo.Role> roles = authorityVo.getAuthorities();
            for (AuthorityVo.Role role : roles) {
                if ("ADMIN".equals(role.getAuthority())) {
                    log.info("是管理员,允许访问");
                    return true;
                }
            }
        }
        log.info("非管理员，不允许访问");
        throw new AuthenticationException("你没有权限！");


    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    @Override
    public void setEnvironment(Environment environment) {
        log.info("user center url is {}", environment.getProperty("user.center.url"));
        url = environment.getProperty("user.center.url");
    }
}
