create table info(
  id BIGINT AUTO_INCREMENT, #主键ID
  title  VARCHAR(100),  #资讯标题
  name  VARCHAR(20),   #资讯作者
  date  VARCHAR(30),   #资讯编写日期
  content TEXT,       #资讯内容
  album TEXT,         #照片路径
  sort BIGINT,        #排序标识
  flag INT,           #是否展览
  PRIMARY KEY(id)
)
  CHARACTER SET=utf8;



  insert into info(title,name,date,content,album,flag,sort) values('hello','chen','1993-12-30','你好','/user/album',0,1);
  insert into info(title,name,date,content,album,flag,sort) values('world','tao','1993-12-30','欢迎','/user/album',1,2);
  insert into info(title,name,date,content,album,flag,sort) values('we are the world','yang','1991-03-12','we are family','/user/album',1,3);
  insert into info(title,name,date,content,album,flag,sort) values('children','kobe','1992-02-13','good','/user/album',1,4);
  insert into info(title,name,date,content,album,flag,sort) values('spring','curry','1893-07-10','hello','/user/album',1,5);